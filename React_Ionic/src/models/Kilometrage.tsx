import { Avion } from "./Avion";

export interface Kilometrage {
    id: number;
    avion: Avion;
    dateKilometrage: Date;
    debutKm: number;
    finKm: number;
}