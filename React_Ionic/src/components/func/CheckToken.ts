const CheckToken = async()=>{
    var token = sessionStorage.getItem('tokens');
    console.log(token);
    if (token==null) return false;
    const header=new Headers();
    header.set('tokens', token);
    var response=await fetch('https://railwayavion-production.up.railway.app/admins/token/check',{
        method:'GET',
        headers:header
    });
    console.log("Response="+response);

    var data=await response.json();
    if(data.error==null) return true;
    return false;

}
export default CheckToken;