import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonPage, IonRouterOutlet, setupIonicReact/*,IonTitle, IonToolbar,  IonButtons, IonContent, IonHeader, IonMenu, IonMenuButton, */ } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Login from './pages/Login';
import Header from './components/Header';
import Footer from './components/Footer';
import DetailAvion from './components/DetailAvion';
import ListeAvion from './components/ListeAvionComponent';
import ListeAvionAssurance from './components/ListeAvionAssurance';
// import ListeAvion3 from './components/ListeAvionAssurance3';

import './assets/myappcss/App.css';
// import ListeAvion from './pages/ListeAvion';
import Menu from './components/Menu';
import MenuHeader from './components/MenuHeader';
import InfiniteScrollExample from './components/InfiniteScrollExample';
setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="/login">
          <Header />
          <Login />
        </Route>
        <Route path="">
          <Redirect from='/' to="/avions" />
        </Route>
        <Route path="/avions">
          <ListeAvion />
        </Route>
        <Route path="/avions/:id" component={DetailAvion} />
        <Route path="/assurance/:mois" component={ListeAvionAssurance}/>
        <Route path="/scroll">
          <InfiniteScrollExample />
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
