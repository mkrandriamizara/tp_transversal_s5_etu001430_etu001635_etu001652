-- CREATE USER avion WITH PASSWORD 'avion';
-- CREATE DATABASE avion;
-- ALTER DATABASE avion OWNER TO avion;

CREATE TABLE Admin
(
    id SERIAL PRIMARY KEY ,
    nomAdmin VARCHAR(255) NOT NULL ,
    mdpAdmin VARCHAR(32) NOT NULL ,
    email VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Avion
(
    id SERIAL PRIMARY KEY ,
    idOrigine INTEGER ,
    Matricule INTEGER ,
    nomAvion VARCHAR(255) NOT NULL ,
    description VARCHAR(255) NOT NULL    
);

CREATE TABLE Origine
(
    id SERIAL PRIMARY KEY,
    Origine VARCHAR(255) NOT NULL
);

CREATE TABLE Kilometrage(
    id SERIAL PRIMARY KEY ,
    idAvion INTEGER NOT NULL,
    dateKilometrage DATE NOT NULL,
    debutKm DOUBLE PRECISION NOT NULL,
    finKm DOUBLE PRECISION NOT NULL
);

CREATE TABLE Assurance(
    id SERIAL PRIMARY KEY ,
    libelle VARCHAR(255)
);

CREATE TABLE AssuranceAvion(
    id SERIAL PRIMARY KEY ,
    idAvion INTEGER NOT NULL,
    idAssurance INTEGER NOT NULL,
    dateDebut DATE NOT NULL,
    dateFin DATE NOT NULL
);

CREATE TABLE Entretien(
    id SERIAL PRIMARY KEY ,
    typeEntretien VARCHAR(255)  NOT NULL
);

CREATE TABLE EntretienAvion(
    id SERIAL PRIMARY KEY ,
    idEntretien INTEGER NOT NULL,
    idAvion INTEGER NOT NULL,
    dateEntretien DATE NOT NULL
);

/*ALTER TABLE*/

ALTER TABLE Avion
    ADD FOREIGN KEY (idOrigine) REFERENCES Origine(id);

ALTER TABLE Kilometrage
    ADD FOREIGN KEY (idAvion) REFERENCES Avion(id);

ALTER TABLE AssuranceAvion
    ADD
        FOREIGN KEY (idAvion) REFERENCES Avion(id),
    ADD
        FOREIGN KEY (idAssurance) REFERENCES Assurance(id);

ALTER TABLE EntretienAvion
    ADD
        FOREIGN KEY (idEntretien) REFERENCES Entretien(id),
    ADD
        FOREIGN KEY (idAvion) REFERENCES Avion(id);




/*INSERTION*/

INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'Russie');
INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'USA');
INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'France');
INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'Bresil');
INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'Royaume-Uni');
INSERT INTO Origine(id, Origine) VALUES(DEFAULT,'Allemagne');
INSERT INTO Origine(id,Origine) VALUES(DEFAULT,'Chine');
INSERT INTO Origine(id,Origine) VALUES(DEFAULT,'Canada');
INSERT INTO Origine(id,Origine) VALUES(DEFAULT,'Ukraine');
INSERT INTO Origine(id,Origine) VALUES(DEFAULT,'UE');
/*10*/

INSERT INTO Origine(id,Origine) VALUES(DEFAULT,'');

INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(1,'Iliouchine Il-76 Candid','600 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(1,'Iliouchine Il-96','262 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(2,'Boeing 747-400 Large Cargo Freighter Dreamlifter','660 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(2,'Boeing 787 Dreamliner','246 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(3,'ATR 72-500','90 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(3,'Fournier RF-9','Biplace');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(4,'Embraer 190-E2','114 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(4,'Embraer 195','132 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(5,'BAe 146','111 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(5,'Britten-Norman Trislander','18 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(6,'MBB 223 Flamingo','3 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(6,'Fairchild-Dornier 328','34 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(7,'C919','168 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(7,'ARJ21','90 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(8,'CS300','110 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(8,'CRJ-900','90 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(10,'A350 XWB-900/1000','314 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(9,'An-124 Condor','88 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(10,'A319','150 sièges');
INSERT INTO Avion(idOrigine,nomAvion,description) VALUES(10,'A320neo','146 sièges');


INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(1,'2021-01-25','2021-11-30');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(2,'2020-11-12','2022-12-05');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(3,'2019-07-15','2022-01-30');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(4,'2021-09-13','2022-12-20');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(5,'2021-02-22','2025-02-23');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(6,'2021-02-22','2025-02-23');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(7,'2021-02-22','2025-02-23');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(8,'2020-03-20','2025-05-10');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(9,'2019-04-22','2025-05-10');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(10,'2022-03-15','2026-05-03');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(11,'2022-07-13','2026-05-05');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(12,'2022-07-18','2026-05-05');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(13,'2019-06-10','2023-06-10');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(14,'2019-02-08','2026-02-14');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(15,'2017-01-30','2024-01-30');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(16,'2021-08-25','2028-08-25');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(17,'2021-10-12','2026-10-12');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(18,'2022-12-16','2029-12-28');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(19,'2021-12-31','2028-12-31');
INSERT INTO Assurance(idAvion,dateDebut,dateFin) VALUES(20,'2022-06-25','2029-06-25');


INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(1,'2022-12-20','15000','15225');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(2,'2022-12-19','4500','4789');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(3,'2022-11-10','26000','26500');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(4,'2022-11-17','12550','12750');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(5,'2022-11-17','15000','15600');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(6,'2022-12-13','6245','6657');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(7,'2022-12-20','8545','9000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(8,'2022-11-30','12548','13560');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(9,'2022-11-30','17000','17250');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(10,'2022-11-30','28000','30000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(12,'2022-11-30','15800','17000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(13,'2022-11-30','13546','15000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(14,'2022-11-30','19250','23545');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(15,'2022-11-30','35500','36750');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(16,'2022-11-30','14000','15000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(17,'2022-11-30','13500','17000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(18,'2022-12-28','17800','18995');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(19,'2022-12-28','18000','20000');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(11,'2022-12-28','45000','47500');
INSERT INTO Kilometrage(idAvion,dateKilometrage,debutKm,finKm) VALUES(20,'2022-12-28','35980','38000');

INSERT INTO Entretien(typeEntretien) VALUES('Vidange');

INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,1,'2022-12-20');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,2,'2022-12-19');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,3,'2022-12-18');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,5,'2022-12-17');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,8,'2022-12-16');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,6,'2022-12-15');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,7,'2022-12-17');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,10,'2022-01-31');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,20,'2022-07-15');
INSERT INTO EntretienAvion(idEntretien,idAvion,dateEntretien) VALUES(1,19,'2022-02-12');

/*select*/

Select * from Avion;



INSERT INTO Avion(matricule) VALUES('AV0001');
INSERT INTO Avion(matricule) VALUES('AV0002');
INSERT INTO Avion(matricule) VALUES('AV0003');
INSERT INTO Avion(matricule) VALUES('AV0004');
INSERT INTO Avion(matricule) VALUES('AV0005');
INSERT INTO Avion(matricule) VALUES('AV0006');
INSERT INTO Avion(matricule) VALUES('AV0007');
INSERT INTO Avion(matricule) VALUES('AV0008');
INSERT INTO Avion(matricule) VALUES('AV0009');
INSERT INTO Avion(matricule) VALUES('AV0010');
INSERT INTO Avion(matricule) VALUES('AV0011');
INSERT INTO Avion(matricule) VALUES('AV0012');
INSERT INTO Avion(matricule) VALUES('AV0013');
INSERT INTO Avion(matricule) VALUES('AV0014');
INSERT INTO Avion(matricule) VALUES('AV0015');
INSERT INTO Avion(matricule) VALUES('AV0016');
INSERT INTO Avion(matricule) VALUES('AV0017');
INSERT INTO Avion(matricule) VALUES('AV0018');
INSERT INTO Avion(matricule) VALUES('AV0019');
INSERT INTO Avion(matricule) VALUES('AV0020');