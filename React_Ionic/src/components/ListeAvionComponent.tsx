import {
  IonPage,
  IonContent
} from '@ionic/react'
import React, { useState, useEffect/*,ReactNode*/ } from 'react';
import { Avion } from '../models/Avion';
import Menu from './Menu';
import MenuHeader from './MenuHeader';
import AvionComp from './Avion';


const ListeAvion = () => {
  const [avions, setAvions] = useState([]);

  type AvionComp = {
    id: number,
    matricule: string,
  }

  useEffect(() => {
    fetch('https://railwayavion-production.up.railway.app/avions')
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setAvions(data.data);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);
  /*const pushData = () => {
    if(avions.length===allAvions.length){
        setInfiniteDisabled(true);
    }
    const max = avions.length + 10;
    const min = max - 10;
    const newData = [];
    for (let i = min; i < max; i++) {
        if(allAvions[i]==null || allAvions[i]==undefined){
            continue;
        }
      newData.push(allAvions[i]);
    }
  }
  const loadData = (ev: any) => {
    setTimeout(() => {
      pushData();
      console.log('Loaded data');
      ev.target.complete();
    }, 500);
  }  
  
  useIonViewWillEnter(() => {
    pushData();
  });*/

  return (
    <div className="posts-container">
      <Menu />
      <IonPage id="main-content">
        <MenuHeader />
        <IonContent className="ion-padding">
          {avions.map((avion: AvionComp, index: number) => <AvionComp key={index} id={avion.id} matricule={avion.matricule}/>)}
        </IonContent>
      </IonPage>
    </div>
  );
}

export default ListeAvion