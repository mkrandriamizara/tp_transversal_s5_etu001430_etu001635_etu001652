import { IonButton, IonCheckbox, IonInput, IonItem, IonLabel, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../assets/myappcss/Form.css';
type Props = {}

const FormLogin = (props: Props) => {
    const history = useHistory();
    var emailRef = useRef<HTMLIonInputElement>(null);
    var passwordRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();

    function saveTokenOnStorage(token: string) {
        sessionStorage.setItem('tokens', token);
    }

    function removeTokenOnStorage() {
        sessionStorage.removeItem('tokens');
    }
    function authentificate() {
        var email = emailRef.current?.value;
        var password = passwordRef.current?.value;
        var content = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email: email, mdpadmin: password })
        }
        fetch('https://railwayavion-production.up.railway.app/login', content).then(response =>
            response.json()
        ).then((data) => {
            if (data.data == null) {
                presentAlert({
                    header: 'Sign in failed',
                    message: data.error.code + "-" + data.error.message,
                    buttons: ['OK'],
                })
            }
            else {
                console.log(data);
                removeTokenOnStorage();
                saveTokenOnStorage(data.data.value);
                history.push('/liste');
            }
        }).catch((error) => {
            presentAlert({
                header: 'Sign in failed',
                message: error,
                buttons: ['OK'],
            })
        })
    }
    return (
        <form className="ion-padding myform">
            <IonLabel className='formtitle'>Se connecter</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Email</IonLabel>
                <IonInput ref={emailRef} />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput ref={passwordRef} type="password" />
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={authentificate}>
                Login
            </IonButton>
                <p className='soratra'><b>Email: </b>admin@mail.com</p>
                <p className='soratra'><b>Mdp: </b>admin</p>
        </form>
    )
}

export default FormLogin;