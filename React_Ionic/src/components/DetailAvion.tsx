import { IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonLabel, useIonAlert } from "@ionic/react";
import { useEffect, useState } from "react";
import { Redirect, RouteComponentProps } from "react-router-dom"
import { Kilometrage } from "../models/Kilometrage";
import { Avion } from "../models/Avion";
import CheckToken from "./func/CheckToken";


interface DetailAvionProps extends RouteComponentProps<{
    id: string;
}> { }



const DetailAvion: React.FC<DetailAvionProps> = ({ match, history }) => {
    const [avion, setAvion] = useState<Avion>();
    const [kilometrages, setKilometrages] = useState([]);
    const [token,setTokenResponse] = useState<Boolean>();
    const [presentAlert] = useIonAlert();
    

    useEffect(() => {
        async function fetchToken(){
            const responseToken=await CheckToken();
            console.log(responseToken);
            setTokenResponse(responseToken)
        }
        fetchToken();
        fetch('https://railwayavion-production.up.railway.app/avions/' + match.params.id)
            .then((response) => response.json())
            .then((data) => {
                if (data.data == null) {
                    presentAlert({
                        header: 'Error',
                        message: data.error.code + "-" + data.error.message,
                        buttons: ['OK']
                    })
                }
                else {
                    console.log(data);
                    setAvion(data.data)
                }
            }).catch((err) => {
                console.log(err.message);
            });
    }, []);
    useEffect(() => {
        fetch('https://railwayavion-production.up.railway.app/avions/' + match.params.id + '/kilometrages')
            .then((response) => response.json())
            .then((data) => {
                setKilometrages(data.data)
            })
            .catch((err) => {
                console.log(err.message);
            });;
    })
    console.log(token)
    if(token==false) {
        history.push('/login')
    }
    if(kilometrages!=null){
    return (
        <>
            {/* <IonLabel>
                <h2>Details avion</h2>
            </IonLabel> */}
            <IonCard>
                <IonCardTitle>Details avion</IonCardTitle>
                <b>id:{avion?.id}</b><br />
                <b>Matricule:{avion?.matricule}</b><br />
            </IonCard>
            <h2>Kilometrage</h2>
            {kilometrages.map((kilometrage: Kilometrage) => {
                return(
                    <IonCard key={kilometrage?.id}>
                        <IonCardTitle>Kilometrage du:
                            <>
                            {kilometrage.dateKilometrage}
                            </>
                            </IonCardTitle>
                        <IonCardSubtitle>Debut:{kilometrage.debutKm}</IonCardSubtitle>
                        <IonCardSubtitle>Fin:{kilometrage.finKm}</IonCardSubtitle>
                    </IonCard>
                );
            })
            }
            
        </>
    );}
    else{
        return (
            <>
                {/* <IonLabel>
                    <h2>Details avion</h2>
                </IonLabel> */}
                <IonCard>
                    <IonCardTitle>Details avion</IonCardTitle>
                    <b>id:{avion?.id}</b><br />
                    <b>Matricule:{avion?.matricule}</b><br />
                </IonCard>
                <h2>Kilometrage</h2>
                <p>None</p>
                
            </>
        );
    }
};

export default DetailAvion;