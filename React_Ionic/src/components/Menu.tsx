import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonLabel } from '@ionic/react'
import React from 'react'
import { Redirect } from 'react-router'

type Props = {}

const Menu = (props: Props) => {
    function logout() {
        var token = sessionStorage.getItem('token');
        const header = new Headers();
        if (token == null) window.location.href = '/avions';
        else {
            header.set('token', token);
            localStorage.removeItem('token');
            var response = fetch('https://railwayavion-production.up.railway.app/admins/token/check/', {
                method: 'GET',
                headers: header
            });
            window.location.href = '/avions';
        }
    }
    return (
        <IonMenu contentId="main-content">
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Menu</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className="ion-padding">
                <IonList>
                    <IonItem href="/avions">
                        <IonLabel>Listes avions</IonLabel>
                    </IonItem>
                    {/* <IonItem onClick={logout}>
                        <IonLabel>Deconnexion</IonLabel>
                    </IonItem> */}
                    <IonItem href='/login'>
                        <IonLabel>Login</IonLabel>
                    </IonItem>
                    <IonItem href='/assurance/1'>
                        <IonLabel>Assurance expire 1 mois</IonLabel>
                    </IonItem>
                    <IonItem href='/assurance/3'>
                        <IonLabel>Assurance expire 3 mois</IonLabel>
                    </IonItem>
                </IonList>
            </IonContent>
        </IonMenu>
    )
}

export default Menu