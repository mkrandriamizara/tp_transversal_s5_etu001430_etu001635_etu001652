import { IonCard, IonCardHeader, IonCardTitle, IonCardSubtitle, IonCardContent } from '@ionic/react'
import React, { ReactNode } from 'react'

type Props = {
  id: number,
  matricule: string,
}

const Avion = (props: Props) => {
  return (
    <IonCard key={props.id} button className="avion" href={`/avions/${props.id}`}>
      <IonCardHeader>
        <IonCardTitle>Id:{props.id}</IonCardTitle>
        <IonCardSubtitle>Matricule:{props.matricule}</IonCardSubtitle>
      </IonCardHeader>
    </IonCard>
  )
}

export default Avion